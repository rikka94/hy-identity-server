﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4.AspNetIdentity;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace IsAdmin.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUserProfileService : IProfileService
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public ApplicationUserProfileService (UserManager<ApplicationUser> userManager)
        {
            this._userManager = userManager;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var user = await _userManager.GetUserAsync(context.Subject);


            var claims = new List<Claim>();
            
            if (context.RequestedClaimTypes.Contains("user_id"))
            {
                claims.Add(new Claim("user_id", user.Id));
            }

            if (context.RequestedClaimTypes.Contains("user_name"))
            {
                claims.Add(new Claim("user_name", user.UserName));
            }

            context.IssuedClaims.AddRange(claims);
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var user = await _userManager.GetUserAsync(context.Subject);

            context.IsActive = user != null;
        }
    }
}
